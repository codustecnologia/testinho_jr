//= require jquery
//= require jquery_ujs
//= require ./jquery.slimscroll.min
//= require ./bootstrap.min
//= require ./jquery-flot/jquery.flot
//= require ./jquery-flot/jquery.flot.resize
//= require ./jquery-flot/jquery.flot.pie
//= require ./flot.curvedlines/curvedLines
//= require ./jquery.flot.spline
//= require_tree ./metisMenu
//= require ./iCheck.min
//= require ./jquery.peity.min
//= require ./sparkline
//= require_tree ./scripts
