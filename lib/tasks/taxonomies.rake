namespace :taxonomies do
  desc "Fetch and save taxonomies to database"
  task scrapdata: :environment do
    link_to_mbi = "http://www.mbi.com.br/mbi/biblioteca/tutoriais/classificacao-atividade-economica/"
    document = Nokogiri::HTML(open(link_to_mbi))

    taxonomy = [[], [], [], [], []]
    (1..4).each { |column|
      css_path = "table:first-of-type table:first-of-type tr:first-of-type table:first-of-type tr td:nth-child(#{column})"
      document.css(css_path).each { |cell|
        formated_cell = cell.text.gsub(/\r|\t|\n|\u00A0/, '')
        taxonomy[column] << formated_cell
      }
    }

    LevelOneTaxonomy.delete_all
    LevelTwoTaxonomy.delete_all
    LevelThreeTaxonomy.delete_all
    current_sector_id = 0
    current_subsector_id = 0
    taxonomies = taxonomy[1].length
    (1...taxonomy[1].length).each do |i|
      if taxonomy[3][i].empty?
        if current_sector_id == 0 || LevelOneTaxonomy.find(current_sector_id).name != taxonomy[1][i]
          LevelOneTaxonomy.create(name: taxonomy[1][i])
          current_sector_id = LevelOneTaxonomy.last.id
        end
        LevelTwoTaxonomy.create(name: taxonomy[2][i], explanation: taxonomy[4][i], level_one_taxonomy_id: current_sector_id)
        current_subsector_id = LevelTwoTaxonomy.last.id
      else
        LevelThreeTaxonomy.create(name: taxonomy[3][i], explanation: taxonomy[4][i], level_two_taxonomy_id: current_subsector_id)
      end
    end
  end
end
