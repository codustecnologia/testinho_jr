# encoding: utf-8
class Admin < ActiveRecord::Base

  # devise
  devise :database_authenticatable, :validatable, :recoverable, :rememberable

  # validations
  validates :name, presence: true
  validates :email, presence: true

end
