# encoding: utf-8

module Shared::CodusTemplateHomer
  module Base
    include CodusTemplates::Helpers::Base

    def menu_active_for(sym, actions = nil)
      super(sym, actions).gsub("open", "")
    end

    def display_flash_message
      return if flash.empty?
      alerts = flash.inject('') do |content, (key, message)|
        next unless [:alert, :success, :notice, :error, :danger, :warning, :info].include?(key.to_sym)
        key = :success if key.to_sym == :notice
        key = :danger if key.to_sym.in? [:error, :alert]

        alert = content_tag :div, :class => "alert alert-#{key}" do
          content_tag(:p, message) unless message.blank?  || key.to_sym == :timedout
        end.html_safe
        content << alert.html_safe
      end
      alerts.html_safe if alerts
    end

    def header_section(custom_options = {})
      options = {
        title: get_title_text_for_header_section,
        right_links: get_right_links_for_header_section,
        header_tag: :h2
      }.merge(custom_options)

        # .hpanel
        #   .panel-body
        #     h2 .font-light.m-b-xs
        #       | Forms elements

      content_tag(:div, class: 'normalheader transition animated fadeIn') do
        content_tag(:div, class: 'hpanel') do
          content_tag(:div, class: 'panel-body') do
            title = content_tag(options[:header_tag], "#{options[:title]}".html_safe, class: "font-light m-b-xs")
            (title).html_safe
          end
        end
      end
    end

    def get_right_links_for_header_section
      begin
        right_links = link_to(get_translation_for_button(:back), {action: 'index'}, {class: 'btn btn-default'})
      rescue
        right_links = ""
      end
      if params[:action].match(/index/)
        begin
          right_links = link_to("#{get_translation_for_button(:add)}".html_safe, { action: :new }, class: 'btn btn-primary cpy-add-link')
        rescue
          right_links = ""
        end
      end

      right_links
    end

    def form_submit_section(custom_options = {})
      options = {
        back_button_name: get_translation_for_button(:back),
        submit_button_name: get_translation_for_button(:save)
      }.merge(custom_options)
      content_tag(:div, class: 'form-group') do
        content_tag(:div, class: 'col-sm-8 col-sm-offset-2') do
          begin
            back_link = link_to("#{options[:back_button_name]}".html_safe, {action: 'index'}, {class: 'btn btn-default'})
          rescue
            back_link = link_to("#{options[:back_button_name]}".html_safe, :back, {class: 'btn btn-default'})
          end
          submit_button = "<button class='btn btn-primary cpy-submit-button'> #{options[:submit_button_name]}</button> ".html_safe
          (submit_button + back_link).html_safe
        end
      end
    end

    def show_buttons_section(custom_options = {})
      options = {
        back_button_name: get_translation_for_button(:back),
        edit_button_name: get_translation_for_button(:edit)
      }.merge(custom_options)

      buttons = content_tag(:div, class: 'form-group') do
        content_tag(:div, class: 'col-sm-8 col-sm-offset-2') do
          if options[:path_to_return]
            back_link = link_to("#{options[:back_button_name]}".html_safe, options[:path_to_return].to_sym, {class: 'btn btn-default'})
          else
            begin
              back_link = link_to("#{options[:back_button_name]}".html_safe, {action: 'index'}, {class: 'btn btn-default'})
            rescue
              back_link = link_to("#{options[:back_button_name]}".html_safe, :back, {class: 'btn btn-default'})
            end
          end
          begin
            edit_link = link_to("#{options[:edit_button_name]}".html_safe, {action: 'edit'}, {class: 'btn btn-primary'})
          rescue
            edit_link = ''
          end

          begin
            edit_link = link_to("#{options[:edit_button_name]}".html_safe, {action: 'edit'}, {class: 'btn btn-primary'})
          rescue
            edit_link = ''
          end

          back_link + edit_link
        end
      end
      buttons.html_safe
    end

    def date_input(options)
      f = options.delete(:form_builder)
      attribute = options.delete(:attribute)
      input_options = {
        as: :string
      }.merge(options.fetch(:input_options, {}))
      input_options[:class] = "form-control js-mask-date #{input_options[:class].to_s}"
      extra_options = {}
      if options.has_key?(:wrapper)
        extra_options[:wrapper] = options[:wrapper]
      end

      f.input(attribute, extra_options) do
        content_tag(:div, class: 'input-group date') do
          input_options[:value] = f.object.send(attribute).nil? ? '' : I18n.l(f.object.send(attribute), format: '%d/%m/%Y')
          input = f.input_field(attribute, input_options)
          calendar_icon = content_tag(:span, class: 'input-group-addon') do
            content_tag(:i, '', { class: 'glyphicon glyphicon-th'} )
          end
          input + calendar_icon
        end
      end
    end

    def pagination_themed(resource_list, options = {})
      final_options = {
        :theme => 'ace_template'
      }.merge(options)
      paginate(resource_list, final_options)
    end
  end
end
