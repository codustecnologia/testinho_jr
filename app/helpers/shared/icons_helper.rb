module Shared::IconsHelper
  def icon_for(icon_type)
    selected_icon_class = Hash.new {|hash, key| hash[key] = "fa fa-#{key}" }
    selected_icon_class.merge(
      show: 'fa fa-eye',
      edit: 'fa fa-pencil',
      plus: 'fa fa-plus',
      back: 'fa fa-arrow-left',
      question: 'fa fa-question-circle',
      user: 'pe-7s-user',
      mail: 'pe-7s-mail',
      signout: 'sign-out'
    )
    "<i class='#{selected_icon_class[icon_type]}'></i>".html_safe
  end
end
