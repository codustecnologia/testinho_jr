class Admin::CategoriesController < Admin::BaseController
  def index
    @categories = Category.all.page(params[:page])
  end

  def new
    @category = Category.new
  end

  def edit
    @category = Category.find(params[:id])
  end

  def create
    @category = Category.create(permitted_params)
    respond_with(@category, location: admin_categories_path)
  end

  def update
    @category = Category.find(params[:id])
    @category.update_attributes(permitted_params)
    respond_with(@category, location: admin_category_path)
  end

  def show
    @category = Category.find(params[:id])
    respond_with @category
  end


  def destroy
    @category = Category.find(params[:id])
    @category.destroy
    respond_with(@category, location: admin_categories_path)
  end


  private
    def permitted_params
      params.require(:category).permit!
    end

end

