# encoding: utf-8
class Admin::Authentication::SessionsController < ::Devise::SessionsController
  layout 'login'

  def after_sign_in_path_for(admin)
    admin_categories_path
  end

  def after_sign_out_path_for(resource_or_scope)
    new_admin_session_path
  end

end
