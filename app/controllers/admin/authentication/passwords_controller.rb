# encoding: utf-8
class Admin::Authentication::PasswordsController < ::Devise::PasswordsController
  layout 'login'

  def after_resetting_password_path_for(admin)
    admin_categories_path
  end

end
