# encoding: utf-8
class Admin::BaseController < ApplicationController
  protect_from_forgery
  layout 'admin/main'
  before_filter :authenticate_admin!
  respond_to :html
  responders :flash
end
