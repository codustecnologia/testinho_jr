# encoding: utf-8
class Public::BaseController < ApplicationController
  protect_from_forgery
  layout 'public/main'

end
