require File.expand_path('../boot', __FILE__)

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TestinhoJr
  class Application < Rails::Application


    config.time_zone = 'Brasilia'
    config.i18n.locale = :"pt-BR"
    config.i18n.default_locale = :"pt-BR"
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    # Compile
    config.assets.precompile += %w( homer/init.css init_public.css init_admin.css )
    config.assets.precompile += %w( init_public.js init_admin.js )
    config.assets.paths << "#{Rails.root}/vendor/assets/fonts"
    config.assets.paths << "#{Rails.root}/vendor/assets/images"
    config.assets.precompile << /\.(?:svg|eot|woff|woff2|ttf)$/
    config.assets.precompile += %w[ *.png *.jpeg *.jpg *.gif ]

    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
  end
end
