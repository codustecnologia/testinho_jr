# encoding: utf-8
class DevelopmentMailInterceptor
  def self.delivering_email(message)
    message.subject = "[ #{Rails.env} - #{message.to} - #{message.bcc}] #{message.subject}"
    message.to = ENV['REDIRECT_INTERCEPTED_EMAILS_TO']
    message.bcc = ENV['REDIRECT_INTERCEPTED_EMAILS_TO']
  end
end

if (Rails.env.development? || ENV['INTERCEPT_EMAILS'] == 'true')
  ActionMailer::Base.register_interceptor(DevelopmentMailInterceptor)
end
