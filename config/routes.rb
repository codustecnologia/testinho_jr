Rails.application.routes.draw do
  root to: 'public/home#show'

  devise_for :admins,
    controllers: {
      sessions:           'admin/authentication/sessions',
      passwords:          'admin/authentication/passwords'
    }

  namespace :public, path: '/' do
    resources :home, path: '', only: [:show]
  end

  namespace :admin do
    resources :categories
  end

end
